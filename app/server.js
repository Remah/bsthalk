const express = require('express');
const logger = require('morgan');
const bodyparser = require('body-parser');
const cors = require('cors');
const helmet = require('helmet');
const bearerToken = require('express-bearer-token');

const middlewares = require('./middlewares');
const routes = require('./routes');

const app = express();
app.use(logger('dev'));
app.use(bodyparser.json({ limit: '50mb' }));
app.use(
  bodyparser.urlencoded({
    limit: '50mb',
    extended: true,
    parameterLimit: 50000
  })
);
app.use(cors());
app.use(helmet());
app.use(bearerToken());
app.use(middlewares.ensureContentType);
app.use(middlewares.apiKeyChecker);
app.use(middlewares.jwtAuthChecker);
app.use(routes);
app.use(middlewares.errorHandler);

module.exports = {
  up: cb => {
    let server = app.listen(process.env.PORT);
    server.on('listening', cb);
    server.on('error', function (err) {
      console.error(err.message.red);
    });
  }
};
