const models = require('../../../models');
const $baseEntity = require('./$baseEntity');

module.exports = $baseEntity('subjectPairs', async teachers => {
  let exports = {};
  teachers.forEach(teacher => (exports[teacher.id] = []));

  let subjectPair11 = await models
    .subjectPair({
      teacher: teachers[0].id,
      subject: 1, // arabic
      level: 10 //  1st secondary
    })
    .save();
  exports[teachers[0].id].push(subjectPair11);

  // first pair for teachers
  let subjectPair21 = await models
    .subjectPair({
      teacher: teachers[1].id,
      subject: 1, // arabic
      level: 10 //  1st secondary
    })
    .save();
  exports[teachers[1].id].push(subjectPair21);

  let subjectPair31 = await models
    .subjectPair({
      teacher: teachers[2].id,
      subject: 2, // english
      level: 10 //  1st secondary
    })
    .save();
  exports[teachers[2].id].push(subjectPair31);

  let subjectPair41 = await models
    .subjectPair({
      teacher: teachers[3].id,
      subject: 11, // geography
      level: 10 //  1st secondary
    })
    .save();
  exports[teachers[3].id].push(subjectPair41);

  let subjectPair51 = await models
    .subjectPair({
      teacher: teachers[4].id,
      subject: 8, // chemistry
      level: 10 //  1st secondary
    })
    .save();
  exports[teachers[4].id].push(subjectPair51);

  // second pair for teachers
  let subjectPair12 = await models
    .subjectPair({
      teacher: teachers[0].id,
      subject: 1, // arabic
      level: 11 // 2nd secondary
    })
    .save();
  exports[teachers[0].id].push(subjectPair12);

  let subjectPair22 = await models
    .subjectPair({
      teacher: teachers[1].id,
      subject: 1, // arabic
      level: 11 // 2nd secondary
    })
    .save();
  exports[teachers[1].id].push(subjectPair22);

  let subjectPair32 = await models
    .subjectPair({
      teacher: teachers[2].id,
      subject: 2, // english
      level: 11 // 2nd secondary
    })
    .save();
  exports[teachers[2].id].push(subjectPair32);

  let subjectPair42 = await models
    .subjectPair({
      teacher: teachers[3].id,
      subject: 11, // geography
      level: 14 // 2nd secondary
    })
    .save();
  exports[teachers[3].id].push(subjectPair42);

  let subjectPair52 = await models
    .subjectPair({
      teacher: teachers[4].id,
      subject: 8, // chemistry
      level: 12 // 2nd secondary
    })
    .save();
  exports[teachers[4].id].push(subjectPair52);

  // third pair for teachers
  let subjectPair13 = await models
    .subjectPair({
      teacher: teachers[0].id,
      subject: 1, // arabic
      level: 15 // 3rd secondary
    })
    .save();
  exports[teachers[0].id].push(subjectPair13);

  let subjectPair23 = await models
    .subjectPair({
      teacher: teachers[1].id,
      subject: 1, // arabic
      level: 15 // 3rd secondary
    })
    .save();
  exports[teachers[1].id].push(subjectPair23);

  let subjectPair33 = await models
    .subjectPair({
      teacher: teachers[2].id,
      subject: 2, // english
      level: 15 // 3rd secondary
    })
    .save();
  exports[teachers[2].id].push(subjectPair33);

  let subjectPair43 = await models
    .subjectPair({
      teacher: teachers[3].id,
      subject: 11, // geography
      level: 18 // 3rd secondary
    })
    .save();
  exports[teachers[3].id].push(subjectPair43);

  let subjectPair53 = await models
    .subjectPair({
      teacher: teachers[4].id,
      subject: 8, // chemistry
      level: 16 // 3rd secondary
    })
    .save();
  exports[teachers[4].id].push(subjectPair53);

  return exports;
});
