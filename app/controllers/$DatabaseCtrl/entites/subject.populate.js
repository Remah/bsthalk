const models = require('../../../models');
const $baseEntity = require('./$baseEntity');

// { name: 'لغة عربية', icon: 'https://goo.gl/qcMdwE' },
// { name: 'لغة فرنسية', icon: 'https://goo.gl/qcMdwE' },
// { name: 'لغة ألمانية', icon: 'https://goo.gl/qcMdwE' },
// { name: 'لغة إيطالية', icon: 'https://goo.gl/qcMdwE' },
// { name: 'رياضيات', icon: 'https://goo.gl/qcMdwE' },
// { name: 'فزياء', icon: 'https://goo.gl/qcMdwE' },

module.exports = $baseEntity('subjects', async () => {
  let exports = [];
  let baseUrl = 'https://res.cloudinary.com/derossy-backup/image/upload';

  let subjects = [
    {
      name: 'لغة عربية',
      icon: baseUrl + '/v1555207330/deross-samples/subjects/arabic.png'
    }, // index: 1
    {
      name: 'لغة إنجليزية',
      icon: baseUrl + '/v1555207330/deross-samples/subjects/english.png'
    }, // index: 2
    { name: 'لغة فرنسية', icon: 'https://goo.gl/qcMdwE' },
    { name: 'لغة ألمانية', icon: 'https://goo.gl/qcMdwE' },
    { name: 'رياضيات', icon: 'https://goo.gl/qcMdwE' },
    {
      name: 'دراسات الاجتماعية',
      icon: baseUrl + '/v1555207331/deross-samples/subjects/geo.png'
    },
    {
      name: 'علوم',
      icon: baseUrl + '/v1555207330/deross-samples/subjects/chem.png'
    },
    {
      name: 'كمياء',
      icon: baseUrl + '/v1555207330/deross-samples/subjects/chem.png'
    }, // index: 8
    { name: 'فزياء', icon: 'https://goo.gl/qcMdwE' },
    {
      name: 'أحياء',
      icon: baseUrl + '/v1555207331/deross-samples/subjects/bio.png'
    },
    {
      name: 'جغرافيا',
      icon: baseUrl + '/v1555207331/deross-samples/subjects/geo.png'
    }, // index: 11
    {
      name: 'تاريخ',
      icon: baseUrl + '/v1555207331/deross-samples/subjects/hisor.png'
    }
  ];

  for (let subject of subjects) {
    let x = await models.subject(subject).save();
    exports.push(x);
  }

  return exports;
});
