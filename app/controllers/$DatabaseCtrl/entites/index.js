module.exports = {
  level: require('./level.populate'),
  subject: require('./subject.populate'),
  admin: require('./admin.populate'),
  teacher: require('./teacher.populate'),

  subjectPair: require('./subjectPair.populate'),
  group: require('./group.populate'),

  parent: require('./parent.populate'),
  student: require('./student.populate'),
  attendanceReport: require('./attendanceReport.populate'),

  chapter: require('./chapter.populate'),
  tutorial: require('./tutorial.populate'),

  post: require('./post.populate'),

  examAndHomework: require('./exam-homework.populate'),
  solution: require('./solution.populate'),

  question: require('./question.populate')
};
