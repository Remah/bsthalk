const models = require('../../../models');
const $baseEntity = require('./$baseEntity');

module.exports = $baseEntity('levels', async () => {
  let exports = [];
  let baseUrl = 'https://res.cloudinary.com/derossy-backup/image/upload';

  // paragraph
  for (let i = 0; i < 15; i++) {
    let paragraphQ = await new models.paragraph({
      chapter: 1,
      head: 'ماذا أدركت سميرة موسي ؟ وما القرار الذي اتخذته ؟',
      images: [
        {
          url:
            baseUrl + '/v1555207236/deross-samples/questions/samira_mousa.jpg'
        }
      ],
      modelAnswer:
        'أدركت سميرة موسي أن العلم هو الهدف والرسالة والحياة والقرار الذي اتخذته أن العلم أولي من كل شئ وأي شئ'
    }).save();
  }

  // complete
  for (let i = 0; i < 15; i++) {
    let completeQ = await new models.complete({
      chapter: 1,
      head: 'اكمل مكان النقط: سؤال ... سؤال ... سؤال ...',
      numberOfInputs: 3,
      labels: ['1', '2', '3'],
      images: [
        {
          url:
            baseUrl + '/v1555207236/deross-samples/questions/samira_mousa.jpg'
        }
      ],
      modelAnswer: ['اكمل', 'اكمل', 'اكمل']
    }).save();
  }

  // trutefalse
  for (let i = 0; i < 15; i++) {
    let trutefalse = await new models.truefalse({
      chapter: 1,
      head: 'سؤال صح وخطأ',
      images: [
        {
          url:
            baseUrl + '/v1555207236/deross-samples/questions/samira_mousa.jpg'
        }
      ],
      modelAnswer: true
    }).save();
  }

  // choose
  for (let i = 0; i < 15; i++) {
    let choose = await new models.choose({
      chapter: 1,
      head: 'سؤال اختياري',
      choices: ['اختيار 1', 'اختيار 2', 'اختيار 3', 'اختيار 4'],
      images: [
        {
          url:
            baseUrl + '/v1555207236/deross-samples/questions/samira_mousa.jpg'
        }
      ],
      modelAnswer: 1
    }).save();
  }

  // complex
  let complexSubQ = await (async function() {
    let subQ1 = await new models.complete({
      head: 'أعرب ما تحته خط في الفقرة السابقة.',
      numberOfInputs: 4,
      labels: ['الأبناء', 'حملاً', 'يتبرموا', 'التقصير'],
      modelAnswer: [
        'بدل مرفوع، والعلامة الضمة، (وتقبل من الطالب صفة مرفوعة)',
        'خبر كنتم منصوب، والعلامة الفتحة',
        'فعل مضارع مجزوم بحذف النون؛ لأنه من الأفعال الخمسة، وواو الجماعة ضمير مبني في محل رفع فاعل',
        'مبتدأ مؤخر مرفوع، والعلامة الضمة'
      ]
    }).save();

    let subQ2 = await new models.complete({
      head: 'استخرج من الفقرة السابقة ما يلي:',
      numberOfInputs: 3,
      labels: [
        'منادي، وأعربه',
        'أسلوباً للمدح، وحدد المخصوص بالمدح',
        'بدلاً، وبين نوعه'
      ],
      modelAnswer: [
        '(يا أيها): "أي" منادي مبني علي الضم في محل نصب، الهاء للتنبيه',
        '(فنعم عملاً بر الوالدين)، المخصوص بالمدح (بر الوالدين)',
        'الحمل، مطابق'
      ]
    }).save();

    let subQ3 = await new models.complete({
      head: 'أجب بما هوا مطلوب بين القوسين',
      numberOfInputs: 2,
      labels: [
        'يعجبني تراحم المؤمنين. [اجعل الفاعل بدلا في الجملة السابقة، مع كتابة الجملة الصحيحة]',
        'يا غافلون، أفيقوا. [اجعل المنادي نكرة غير مقصودة]'
      ],
      modelAnswer: ['يعجبني المؤمنون تراحمهم', 'يا غافلين أفيقوا']
    }).save();

    let subQ4 = await new models.complete({
      numberOfInputs: 2,
      head:
        '(تحدث مع <u>تلامذة فائقين</u> - تحدثت مع <u>تلاميذ</u> فائقين)<br>- اضبط ما تحته خط في الجملتين السابقتين مع بيان السبب.',
      labels: ['تلامذة', 'تلاميذ'],
      modelAnswer: [
        'تلامذةِ: لأنها اسم مجرور، والعلامة الكسرة',
        'تلاميذَ: لأنها اسم مجرور، والعلامة الفتحة (ممنوع من الصرف)'
      ]
    }).save();

    let subQ5 = await new models.choose({
      head: 'نكشف عن كلمة (كنتم)',
      choices: ['كان', 'كنن', 'كني', 'كون'],
      modelAnswer: 3
    }).save();

    let subQ6 = await new models.truefalse({
      head: 'هل اعجبك الامتحان ؟',
      modelAnswer: true
    }).save();

    return [subQ1.id, subQ2.id, subQ3.id, subQ4.id, subQ5.id, subQ6.id];
  })();

  let complexQ = await models
    .question({
      chapter: 1,
      images: [
        {
          url:
            'https://res.cloudinary.com/derossy-backup/image/upload/v1555207236/deross-samples/questions/shagart_eldoor.png'
        },
        {
          url:
            'https://res.cloudinary.com/derossy-backup/image/upload/v1555207236/deross-samples/questions/samira_mousa.jpg'
        }
      ],
      description:
        '"يا أيها <u>الأبناء</u>، لا تغمضوا عيونكم عن الذين يحيون من أجل توفير الحياة الكريمة لكم، لقد كنتم يوما <u>حملًا</u> ثقيلًا فلم <u>يتبرموا</u> من هذا الحمل، ولم يقصروا في أداء الواجب نحوكم،فهم في حاجة إلي إسعادكم بنجاحكم، فنعم عملاً بر الوالدين، ولا حبذا <u>التقصير</u> في حقهما؛ لتجنوا ثمار ذلك سعادة في حياتكم، ورضا ربكم في الآخرة"',
      subQuestions: complexSubQ
    })
    .save();

  return exports;
});
