const models = require('../../../models');
const $baseEntity = require('./$baseEntity');

module.exports = $baseEntity('admins', async () => {
  let exports = [];
  let baseUrl = 'https://res.cloudinary.com/derossy-backup/image/upload';

  // 1. main admin
  let admin1 = await models
    .admin({
      username: 'admin',
      password: '123456',
      name: 'محمد صلاح',
      gender: 'male',
      email: 'admin@deross.com',
      phone: '01234567891',
      photo: baseUrl + '/v1555206304/deross-samples/male-teacher1.png'
    })
    .save();
  exports.push(admin1);

  // 2. fake admins (for testing multiple admin users)
  let admin2 = await models
    .admin({
      username: 'admin2',
      password: '123456',
      name: 'محمود بكري',
      gender: 'male',
      email: 'admin2@deross.com',
      phone: '01234567881',
      activated: true
    })
    .save();
  exports.push(admin2);

  return exports;
});
