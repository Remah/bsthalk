const models = require('../../../models');
const $baseEntity = require('./$baseEntity');

module.exports = $baseEntity('tutorials', async chapters => {
  let exports = [];

  let videos = [
    {
      url: 'https://www.youtube.com/watch?v=aYhgPVM6sUs',
      title: 'أدوات نصب الفعل المضارع بطريقة ممتعة'
    },
    {
      url: 'https://www.youtube.com/watch?v=mOLifuuNvNo',
      title: 'أسلوب الشرط ـ أدوات الشرط الجازمة'
    },
    {
      url: 'https://www.youtube.com/watch?v=XON30Tfkmw4',
      title: 'اقتران بجواب الشرط بالفاء ــ بطريقة سهلة جدًا'
    },
    {
      url: 'https://www.youtube.com/watch?v=E8idDjfxTTM',
      title: 'كان وأخواتها'
    },
    {
      url: 'https://www.youtube.com/watch?v=hZuUwvOavWk',
      title: 'كان التامة - الفرق بين كان الناقصة والتامة في دقائق'
    }
  ];

  let counter = 1;
  for (let i = 0; i < chapters.length; i++) {
    // x3 times
    for (let k = 0; k < 3; k++) {
      for (let j = 0; j < videos.length; j++) {
        let video = await models
          .tutorial({
            chapter: chapters[i].id,
            title: videos[j].title,
            description: 'وصف مختصر للفيديو',
            type: 'video',
            link: videos[j].url
          })
          .save();
        exports.push(video);

        let pdf = await models
          .tutorial({
            chapter: chapters[i].id,
            title: 'ملف شرح وحدة ' + counter++,
            description: 'وصف مختصر للملف',
            type: 'pdf',
            link:
              'https://res.cloudinary.com/derossy-backup/image/upload/v1555206304/deross-samples/sample.pdf'
          })
          .save();
        exports.push(pdf);
      }
    }
  }

  return exports;
});
