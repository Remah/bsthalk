const models = require('../../../models');
const $baseEntity = require('./$baseEntity');
const $faker = require('../$faker');

module.exports = $baseEntity('students', async (parents, groups) => {
  let exports = [];

  // 1. main student (brother of student2)
  let student1 = await models
    .student({
      username: 'student',
      password: '123456',
      name: 'عمرو ' + parents[0].name,
      gender: 'male',
      email: 'student@deross.com',
      phone: '01234567894',
      photo: $faker.photo('male'),
      parent: parents[0].id,
      level: 10, // 1st secondary
      groups: [
        groups['3']['1'][0].id, // teacher 1 (arabic)
        groups['4']['2'][0].id, // teacher 2 (arabic)
        groups['5']['3'][0].id, // teacher 3 (english)
        groups['6']['4'][0].id, // teacher 4 (geography)
        groups['7']['5'][0].id // teacher 5 (chemistry)
      ]
    })
    .save();

  groups['3']['1'][0].students.push({ student: student1.id });
  groups['4']['2'][0].students.push({ student: student1.id });
  groups['5']['3'][0].students.push({ student: student1.id });
  groups['6']['4'][0].students.push({ student: student1.id });
  groups['7']['5'][0].students.push({ student: student1.id });

  await groups['3']['1'][0].save();
  await groups['4']['2'][0].save();
  await groups['5']['3'][0].save();
  await groups['6']['4'][0].save();
  await groups['7']['5'][0].save();

  exports.push(student1);

  // 2. fake students (brother of student)
  let student2 = await models
    .student({
      username: 'student2',
      password: '123456',
      name: 'أحمد ' + parents[0].name,
      gender: 'male',
      email: 'student2@deross.com',
      phone: '01234557894',
      photo: $faker.photo('male'),
      parent: parents[0].id,
      level: 16 // 3rd secondary
    })
    .save();
  exports.push(student2);

  let counter = 3;
  let lvls = [10, 11, 15];
  let teachersIds = Object.keys(groups);
  let teacherId = teachersIds[0];
  let pairsIds = Object.keys(groups[teacherId]);
  // 3 (pairsIds.length)
  for (let i = 0; i < 2; i++) {
    let pairId = pairsIds[i];
    let pairGroups = groups[teacherId][pairId];
    let parentId = 1; // 2nd element
    // 12 (pairGroups.length)
    for (let j = 0; j < 3; j++) {
      let group = pairGroups[j];
      // 15
      for (let k = 0; k < 15; k++) {
        let gender = $faker.$random.listItem(['male', 'female']);
        let username = 'student' + counter++;
        let student = await models
          .student({
            username: username,
            password: '123456',
            name: $faker.fullName(gender, 1, parents[parentId].name),
            gender: gender,
            email: username + '@deross.com',
            phone: $faker.phone(),
            photo: $faker.photo(gender),
            parent: parents[parentId].id,
            level: lvls[i],
            groups: [group.id]
          })
          .save();
        group.students.push({ student: student.id });
        await group.save();
        exports.push(student);
        parentId++;
      }
    }
  }

  return exports;
});
