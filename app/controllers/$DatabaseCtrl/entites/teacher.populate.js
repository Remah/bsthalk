const models = require('../../../models');
const $baseEntity = require('./$baseEntity');
const $faker = require('../$faker');

module.exports = $baseEntity('teachers', async () => {
  let exports = [];
  let baseUrl = 'https://res.cloudinary.com/derossy-backup/image/upload';

  // 1. main teacher
  // Arabic
  let teacher1 = await models
    .teacher({
      username: 'teacher',
      password: '123456',
      name: 'محمد احمد صلاح',
      gender: 'male',
      email: 'teacher@deross.com',
      phone: '01012345001',
      photo: baseUrl + '/v1555206304/deross-samples/male-teacher1.png',
      premium: true
    })
    .save();
  exports.push(teacher1);

  // 2. fake teachers
  // Arabic
  let teacher2 = await models
    .teacher({
      username: 'teacher2',
      password: '123456',
      name: $faker.fullName('female', 3),
      gender: 'female',
      email: 'teacher2@deross.com',
      phone: $faker.phone(),
      photo: baseUrl + '/v1555206302/deross-samples/female-teacher1.png'
    })
    .save();
  exports.push(teacher2);

  // English
  let teacher3 = await models
    .teacher({
      username: 'teacher3',
      password: '123456',
      name: $faker.fullName('male', 3),
      gender: 'male',
      email: 'teacher3@deross.com',
      phone: $faker.phone(),
      photo: baseUrl + '/v1555206304/deross-samples/male-teacher3.png'
    })
    .save();
  exports.push(teacher3);

  // Geography
  let teacher4 = await models
    .teacher({
      username: 'teacher4',
      password: '123456',
      name: $faker.fullName('male', 3),
      gender: 'male',
      email: 'teacher4@deross.com',
      phone: $faker.phone(),
      photo: baseUrl + '/v1555206304/deross-samples/male-teacher4.png'
    })
    .save();
  exports.push(teacher4);

  // Chemistry
  let teacher5 = await models
    .teacher({
      username: 'teacher5',
      password: '123456',
      name: $faker.fullName('male', 3),
      gender: 'male',
      email: 'teacher5@deross.com',
      phone: $faker.phone(),
      photo: baseUrl + '/v1555206304/deross-samples/male-teacher2.png'
    })
    .save();
  exports.push(teacher5);

  return exports;
});
