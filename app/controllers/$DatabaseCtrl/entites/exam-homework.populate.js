const models = require('../../../models');
const $baseEntity = require('./$baseEntity');
const $faker = require('../$faker');

module.exports = $baseEntity('exams and homeworks', async () => {
  let exports = [];
  let baseUrl = 'https://res.cloudinary.com/derossy-backup/image/upload';

  let subQuestions = await (async function() {
    let subQuestions1 = await (async function() {
      let subQ1 = await new models.paragraph({
        head:
          '"قالت وبريق الأمل يلمع في عينيها: بعزم مولاي تهون الشدائد، وبتوفيق الله تزول العقبات وتنهد الرواسي، وليس من الشجاعة والعزم الصادق صعب، ولا مع الإيمان بالحق مستعصٍ."<br>- فى العبارة السابقة بعض العوامل لتخطي الصعاب. وضح ذلك.',
        modelAnswer:
          'عوامل تخطي الصعاب: توفيق الله - الشجاعة - العزم الصادق - الإيمان بالحق.'
      }).save();

      let subQ2 = await new models.choose({
        head: 'أقصى السلطان الكامل ابنه نجم الدين، وجعله أميراً علي .....',
        choices: [
          'القاهرة في مصر',
          'الثغور في الشام',
          'كيفا على حدود التركستان',
          'الموصل في العراق'
        ],
        modelAnswer: 1
      }).save();

      let subQ3 = await new models.choose({
        head: 'التاجر أبو بكر القماش الذي زار نجم الدين كان قادما من .....',
        choices: ['الشام', 'العراق', 'اليمن', 'مصر'],
        modelAnswer: 3
      }).save();

      let subQ4 = await new models.paragraph({
        head: 'علل: أشارت شجرة الدر على نجم الدين بتكوين جيش من المماليك.',
        modelAnswer:
          'أشارت شجرة الدر علي نجم الدين بتكوين جيش من المماليك، بشراء غلمان ينشئهم علي طاعته، ويملأ قلوبهم بحبه، وأن يأتي بهم صغاراً فيكون لهم الأب والأخ والعم حتي يصل إلي ما يريد.'
      }).save();

      let subQ5 = await new models.paragraph({
        head:
          'ما مبررات شجرة الدر في اختيار القاضي (بدر الدين الزرزاري) ليكون رسولا لاستمالة الخوارزمية؟',
        modelAnswer:
          'المبررات:<br>- قوة بيانه وحسن مداخلة.<br>- مهارته وحسن تصرفه.<br>- قوة(قلبة) ووقوفه بجانب الحق.'
      }).save();

      return [subQ1, subQ2, subQ3, subQ4, subQ5];
    })();

    let subQuestions2 = await (async function() {
      let subQ1 = await new models.choose({
        head: 'مرادف "أولي"',
        choices: ['أقرب وأحق', 'أكبر وأعظم', 'أجمل وأروع', 'أحسن وأفضل'],
        modelAnswer: 0
      }).save();

      let subQ2 = await new models.choose({
        head: 'مضاد "أصرت"',
        choices: ['تعطلت', 'تهاونت', 'تعللت', 'تمارضت'],
        modelAnswer: 1
      }).save();

      let subQ3 = await new models.paragraph({
        head: 'ماذا أدركت سميرة موسي ؟ وما القرار الذي اتخذته ؟',
        images: [
          {
            url:
              baseUrl + '/v1555207236/deross-samples/questions/samira_mousa.jpg'
          }
        ],
        modelAnswer:
          'أدركت سميرة موسي أن العلم هو الهدف والرسالة والحياة والقرار الذي اتخذته أن العلم أولي من كل شئ وأي شئ'
      }).save();

      let subQ4 = await new models.paragraph({
        head: 'علل: مغادرة سميرة موسي مصر إلي إنجلترا.',
        modelAnswer:
          'غادرت سميرة موسي مصر إلي انجلترا للحصول علي درجة الدكتوراه'
      }).save();

      let subQ5 = await new models.paragraph({
        head: 'كيف تسهم في تقديم التقدم والرقي لوطنك ؟',
        modelAnswer:
          'أسهم في تحقيق ذلك باجتهادي في دروسي، وحرصي علي النجاح والتفوق.'
      }).save();

      return [subQ1, subQ2, subQ3, subQ4, subQ5];
    })();

    let subQuestions3 = await (async function() {
      let subQ1 = await new models.choose({
        head: 'مرادف "المعاشرة"',
        choices: ['المعايشة', 'المجالسة', 'المصافحة', 'المقابلة'],
        modelAnswer: 0
      }).save();

      let subQ2 = await new models.choose({
        head: 'مضاد "النافع"',
        choices: ['القبيح', 'المتغير', 'الضار', 'الملوث'],
        modelAnswer: 2
      }).save();

      let subQ3 = await new models.paragraph({
        head: 'للتقوي طرق، وضحها من خلال فهمك للفقرة السابقة.',
        modelAnswer:
          'طرق الوصول للتقوي:<br>1-تهذيب العقل<br>2-تكميل النفس<br>3-حسن المعاملة والمعاشرة<br>4-تعهد النفس بالتربية والتعليم'
      }).save();

      let subQ4 = await new models.paragraph({
        head: 'ما المؤسسة التي تهتم بشئون المرأة والطفل في مصر؟',
        modelAnswer:
          'المجلس القومي للأمومة والطفولة، وكذلك المجلس القومي للمرأة'
      }).save();

      let subQ5 = await new models.paragraph({
        head:
          'قال الشاعر: الأم مدرسة إذا أعددتها   أعددت شعباً طيب الأخلاق<br>ما مدي اتفاق هذا البيت مع النص؟',
        modelAnswer:
          'يتفق معني البيت مع ما يدعو إليه النص، فيجيب علينا إعداد المرأة بتعليمها، وتربيتها التربية السليمة؛ لأن الأم المتعلمة الواعية أقدر علي تربية وتنشئة أجيال واعية، مما ينعكس بالخير علي المجتمع والشعوب؛ فيجعلها قوية متماسكة قادرة علي تحقيق أهدافها'
      }).save();

      let subQ6 = await new models.paragraph({
        head: 'وضح الجمال فيما يلي: (تهذيب العقل، وتكميل النفس)',
        modelAnswer: 'بيت الجملتين تناغم يحدث أثرا موسيقيا حسن الوقع علي الأذن'
      }).save();

      return [subQ1, subQ2, subQ3, subQ4, subQ5, subQ6];
    })();

    let subQuestions4 = await (async function() {
      let subQ1 = await new models.complete({
        head: 'أعرب ما تحته خط في الفقرة السابقة.',
        numberOfInputs: 4,
        labels: ['الأبناء', 'حملاً', 'يتبرموا', 'التقصير'],
        modelAnswer: [
          'بدل مرفوع، والعلامة الضمة، (وتقبل من الطالب صفة مرفوعة)',
          'خبر كنتم منصوب، والعلامة الفتحة',
          'فعل مضارع مجزوم بحذف النون؛ لأنه من الأفعال الخمسة، وواو الجماعة ضمير مبني في محل رفع فاعل',
          'مبتدأ مؤخر مرفوع، والعلامة الضمة'
        ]
      }).save();

      let subQ2 = await new models.complete({
        head: 'استخرج من الفقرة السابقة ما يلي:',
        numberOfInputs: 3,
        labels: [
          'منادي، وأعربه',
          'أسلوباً للمدح، وحدد المخصوص بالمدح',
          'بدلاً، وبين نوعه'
        ],
        modelAnswer: [
          '(يا أيها): "أي" منادي مبني علي الضم في محل نصب، الهاء للتنبيه',
          '(فنعم عملاً بر الوالدين)، المخصوص بالمدح (بر الوالدين)',
          'الحمل، مطابق'
        ]
      }).save();

      let subQ3 = await new models.complete({
        head: 'أجب بما هوا مطلوب بين القوسين',
        numberOfInputs: 2,
        labels: [
          'يعجبني تراحم المؤمنين. [اجعل الفاعل بدلا في الجملة السابقة، مع كتابة الجملة الصحيحة]',
          'يا غافلون، أفيقوا. [اجعل المنادي نكرة غير مقصودة]'
        ],
        modelAnswer: ['يعجبني المؤمنون تراحمهم', 'يا غافلين أفيقوا']
      }).save();

      let subQ4 = await new models.complete({
        numberOfInputs: 2,
        head:
          '(تحدث مع <u>تلامذة فائقين</u> - تحدثت مع <u>تلاميذ</u> فائقين)<br>- اضبط ما تحته خط في الجملتين السابقتين مع بيان السبب.',
        labels: ['تلامذة', 'تلاميذ'],
        modelAnswer: [
          'تلامذةِ: لأنها اسم مجرور، والعلامة الكسرة',
          'تلاميذَ: لأنها اسم مجرور، والعلامة الفتحة (ممنوع من الصرف)'
        ]
      }).save();

      let subQ5 = await new models.choose({
        head: 'نكشف عن كلمة (كنتم)',
        choices: ['كان', 'كنن', 'كني', 'كون'],
        modelAnswer: 3
      }).save();

      return [subQ1, subQ2, subQ3, subQ4, subQ5];
    })();

    let subQuestions5 = await (async function() {
      let subQ1 = await new models.truefalse({
        head: 'هل اعجبك الامتحان ؟',
        modelAnswer: true
      }).save();

      return [subQ1];
    })();

    return [
      subQuestions1,
      subQuestions2,
      subQuestions3,
      subQuestions4,
      subQuestions5
    ];
  })();

  let groups = [{ group: 1, active: true, archived: false }];
  let groups2 = [{ group: 1, active: true, archived: true }];

  let archivedExam = await new models.exam({
    subjectPair: 1,
    title: 'امتحان نحو',
    duration: 30,
    questions: [
      {
        title: 'السؤال الأول<br>قطعة نحو',
        description:
          '"يا أيها <u>الأبناء</u>، لا تغمضوا عيونكم عن الذين يحيون من أجل توفير الحياة الكريمة لكم، لقد كنتم يوما <u>حملًا</u> ثقيلًا فلم <u>يتبرموا</u> من هذا الحمل، ولم يقصروا في أداء الواجب نحوكم،فهم في حاجة إلي إسعادكم بنجاحكم، فنعم عملاً بر الوالدين، ولا حبذا <u>التقصير</u> في حقهما؛ لتجنوا ثمار ذلك سعادة في حياتكم، ورضا ربكم في الآخرة"',
        images: [],
        subQuestions: [
          { question: subQuestions[3][0].id, points: [1, 1, 1, 1] },
          { question: subQuestions[3][1].id, points: [2, 2, 2] },
          { question: subQuestions[3][2].id, points: [2, 2] },
          { question: subQuestions[3][3].id, points: [2, 2] },
          { question: subQuestions[3][4].id, points: 2 }
        ]
      },
      {
        title: 'السؤال الثاني<br>تجريبي',
        description: '',
        images: [],
        subQuestions: [
          { question: subQuestions[3][4].id, points: 2 },
          { question: subQuestions[4][0].id, points: 2 },
          { question: subQuestions[4][0].id, points: 2 },
          { question: subQuestions[2][3].id, points: 4 }
        ]
      }
    ],
    archived: true,
    groups: groups2
  }).save();
  exports.push(archivedExam);

  // exams
  for (let i = 0; i < 15; i++) {
    let exam = await new models.exam({
      subjectPair: 1,
      title: 'امتحان الترم الأول ' + (i + 1),
      duration: 90,
      questions: [
        {
          title: 'السؤال الأول "قراءة"<br>(من قصة طموح جارية)',
          description: '',
          images: [
            {
              url:
                baseUrl +
                '/v1555207236/deross-samples/questions/shagart_eldoor.png'
            }
          ],
          subQuestions: [
            { question: subQuestions[0][0].id, points: 2 },
            { question: subQuestions[0][1].id, points: 2 },
            { question: subQuestions[0][2].id, points: 2 },
            { question: subQuestions[0][3].id, points: 1 },
            { question: subQuestions[0][4].id, points: 1 }
          ]
        },
        {
          title: 'السؤال الثاني "قراءة"<br>(من موضوع سميرة موسي)',
          description:
            '"وأدركت سميرة موسي أن العلم هو الهدف والرسالة والحياة؛ فاتخذت قرارها مبكراً؛ العلم أولي من كل شئ وأي شئ. ثم انتقلت سمير موسي مع أسرتها إلي القاهرة حيث أتمٌت تعليمها الثاني، وأصرت علي دخول الجامعة، وأمام إصرارها وافق والدها علي قرارها"',
          images: [],
          subQuestions: [
            { question: subQuestions[1][0].id, points: 2 },
            { question: subQuestions[1][1].id, points: 1 },
            { question: subQuestions[1][2].id, points: 1 },
            { question: subQuestions[1][3].id, points: 1 },
            { question: subQuestions[1][4].id, points: 1 }
          ]
        },
        {
          title: 'السؤال الثالث "نصوص"<br>(من نص رحمة ومحبة)',
          description:
            '"ولا شئ أدخل في باب التقوي من تهذيب العقل، وتكميل النفس، وإعدادها بالتعليم والتربية إلي معرفة النافع ومدافعة الرذائل، ومقاومة الشهوات، ولا من حسن المعاملة واللطف في المعاشرة"',
          images: [],
          subQuestions: [
            { question: subQuestions[2][0].id, points: 2 },
            { question: subQuestions[2][1].id, points: 2 },
            { question: subQuestions[2][2].id, points: 2 },
            { question: subQuestions[2][3].id, points: 2 },
            { question: subQuestions[2][4].id, points: 2 },
            { question: subQuestions[2][5].id, points: 2 }
          ]
        },
        {
          title: 'السؤال الرابع "نحو"',
          description:
            '"يا أيها <u>الأبناء</u>، لا تغمضوا عيونكم عن الذين يحيون من أجل توفير الحياة الكريمة لكم، لقد كنتم يوما <u>حملًا</u> ثقيلًا فلم <u>يتبرموا</u> من هذا الحمل، ولم يقصروا في أداء الواجب نحوكم،فهم في حاجة إلي إسعادكم بنجاحكم، فنعم عملاً بر الوالدين، ولا حبذا <u>التقصير</u> في حقهما؛ لتجنوا ثمار ذلك سعادة في حياتكم، ورضا ربكم في الآخرة"',
          images: [],
          subQuestions: [
            { question: subQuestions[3][0].id, points: [1, 1, 1, 1] },
            { question: subQuestions[3][1].id, points: [2, 2, 2] },
            { question: subQuestions[3][2].id, points: [2, 2] },
            { question: subQuestions[3][3].id, points: [2, 2] },
            { question: subQuestions[3][4].id, points: 2 }
          ]
        },
        {
          title: 'السؤال الخامس "تجربة "',
          description:
            '"يا أيها <u>الأبناء</u>، لا تغمضوا عيونكم عن الذين يحيون من أجل توفير الحياة الكريمة لكم، لقد كنتم يوما <u>حملًا</u> ثقيلًا فلم <u>يتبرموا</u> من هذا الحمل، ولم يقصروا في أداء الواجب نحوكم،فهم في حاجة إلي إسعادكم بنجاحكم، فنعم عملاً بر الوالدين، ولا حبذا <u>التقصير</u> في حقهما؛ لتجنوا ثمار ذلك سعادة في حياتكم، ورضا ربكم في الآخرة"',
          images: [
            { url: baseUrl + '/v1555207115/deross-samples/posts/post1.png' },
            { url: baseUrl + '/v1555207110/deross-samples/posts/post2.jpg' },
            { url: baseUrl + '/v1555207108/deross-samples/posts/post3.png' },
            { url: baseUrl + '/v1555207114/deross-samples/posts/post4.png' },
            { url: baseUrl + '/v1555207115/deross-samples/posts/post5.png' }
          ],
          subQuestions: [
            { question: subQuestions[4][0].id, points: 2 },
            { question: subQuestions[4][0].id, points: 3 },
            { question: subQuestions[4][0].id, points: 3 },
            { question: subQuestions[4][0].id, points: 3 },
            { question: subQuestions[4][0].id, points: 3 }
          ]
        }
      ],
      groups: i === 0 ? groups : []
    }).save();
    exports.push(exam);
  }

  // homeworks
  for (let i = 0; i < 15; i++) {
    let homework = await new models.homework({
      subjectPair: 1,
      title: 'واجب نحو ' + (i + 1),
      questions: [
        {
          title: 'قطعة نحو',
          description:
            '"يا أيها <u>الأبناء</u>، لا تغمضوا عيونكم عن الذين يحيون من أجل توفير الحياة الكريمة لكم، لقد كنتم يوما <u>حملًا</u> ثقيلًا فلم <u>يتبرموا</u> من هذا الحمل، ولم يقصروا في أداء الواجب نحوكم،فهم في حاجة إلي إسعادكم بنجاحكم، فنعم عملاً بر الوالدين، ولا حبذا <u>التقصير</u> في حقهما؛ لتجنوا ثمار ذلك سعادة في حياتكم، ورضا ربكم في الآخرة"',
          images: [],
          subQuestions: [
            { question: subQuestions[3][0].id, points: [1, 1, 1, 1] },
            { question: subQuestions[3][1].id, points: [2, 2, 2] },
            { question: subQuestions[3][2].id, points: [2, 2] },
            { question: subQuestions[3][3].id, points: [2, 2] },
            { question: subQuestions[3][4].id, points: 2 }
          ]
        }
      ]
    }).save();
    exports.push(homework);
  }

  return exports;
});
