const models = require('../../../models');
const $baseEntity = require('./$baseEntity');

module.exports = $baseEntity(
  'solutions',
  async (examsAndHomeworks, subjectPairs, students) => {
    let exports = [];

    // students of first subjectPair
    let subjectGroups = subjectPairs[3][0].groups.map(g => g.id);
    students = students.filter(student => {
      let found = false;
      subjectGroups.forEach(group => {
        if (student.groups.indexOf(group) !== -1) found = true;
      });
      return found;
    });

    let exam = examsAndHomeworks[0];

    let solution1 = await models
      .solution({
        quiz: exam.id,
        student: students[0].id,
        status: 'done',
        questions: [
          {
            id: exam.questions[0].id,
            subQuestions: [
              {
                id: exam.questions[0].subQuestions[0].id,
                kind: 'complete',
                answer: [
                  'بدل مرفوع بالضمة',
                  '',
                  'فعل مضارع مرفوع بالضمة',
                  'مبتدأ مؤخر مرفوع، والعلامة الضمة'
                ],
                correct: [true, false, false, true],
                mark: [1, 0, 0, 1]
              },
              {
                id: exam.questions[0].subQuestions[1].id,
                kind: 'complete',
                answer: [
                  '(يا أيها): "أي" منادي مبني علي الضم في محل نصب',
                  '',
                  'الحمل، مطابق'
                ],
                correct: [true, false, true],
                mark: [1.5, 0, 2]
              },
              {
                id: exam.questions[0].subQuestions[2].id,
                kind: 'complete',
                answer: ['يعجبني المؤمنون تراحمهم', 'يا غافلين أفيقوا'],
                correct: [true, true],
                mark: [2, 2]
              },
              {
                id: exam.questions[0].subQuestions[3].id,
                kind: 'complete',
                answer: [
                  'تلامذةِ: لأنها اسم مجرور، والعلامة الكسرة',
                  'تلاميذَ: لأنها اسم مجرور، والعلامة الفتحة (ممنوع من الصرف)'
                ],
                correct: [true, true],
                mark: [2, 2]
              },
              {
                id: exam.questions[0].subQuestions[4].id,
                kind: 'choose',
                answer: 0,
                correct: false,
                mark: 0
              }
            ]
          },
          {
            // exam.questions[0].subQuestions[4].id
            id: exam.questions[1].id,
            subQuestions: [
              {
                id: exam.questions[1].subQuestions[0].id,
                kind: 'choose',
                answer: 3,
                correct: true,
                mark: 2
              },
              {
                id: exam.questions[1].subQuestions[1].id,
                kind: 'truefalse',
                answer: false,
                correct: false,
                mark: 0
              },
              {
                id: exam.questions[1].subQuestions[2].id,
                kind: 'truefalse',
                answer: true,
                correct: true,
                mark: 2
              },
              {
                id: exam.questions[1].subQuestions[3].id,
                kind: 'paragraph',
                answer: 'مش عارف',
                correct: false,
                mark: 0
              }
            ]
          }
        ],
        submittedAt: new Date()
      })
      .save();
    exports.push(solution1);

    let solution2 = await models
      .solution({
        quiz: exam.id,
        student: students[1].id,
        status: 'done',
        questions: [
          {
            id: exam.questions[0].id,
            subQuestions: [
              {
                id: exam.questions[0].subQuestions[0].id,
                kind: 'complete',
                answer: [
                  'بدل مرفوع بالضمة',
                  'خبر كنتم منصوب بالفتحة',
                  'فعل مضارع مرفوع بالضمة',
                  'مبتدأ مؤخر مرفوع، والعلامة الضمة'
                ],
                correct: [true, true, false, true],
                mark: [1.5, 1.5, 0, 1.5]
              },
              {
                id: exam.questions[0].subQuestions[1].id,
                kind: 'complete',
                answer: [
                  '(يا أيها): "أي" منادي مبني علي الضم في محل نصب',
                  '',
                  'الحمل، مطابق'
                ],
                correct: [true, false, true],
                mark: [1.5, 0, 2]
              },
              {
                id: exam.questions[0].subQuestions[2].id,
                kind: 'complete',
                answer: ['يعجبني المؤمنون تراحمهم', 'يا غافلين أفيقوا'],
                correct: [true, true],
                mark: [2, 2]
              },
              {
                id: exam.questions[0].subQuestions[3].id,
                kind: 'complete',
                answer: [
                  'تلامذةِ: لأنها اسم مجرور، والعلامة الكسرة',
                  'تلاميذَ: لأنها اسم مجرور، والعلامة الفتحة (ممنوع من الصرف)'
                ],
                correct: [true, true],
                mark: [2, 2]
              },
              {
                id: exam.questions[0].subQuestions[4].id,
                kind: 'choose',
                answer: 0,
                correct: false,
                mark: 0
              }
            ]
          },
          {
            id: exam.questions[1].id,
            subQuestions: [
              {
                id: exam.questions[1].subQuestions[0].id,
                kind: 'choose',
                answer: 3,
                correct: true,
                mark: 2
              },
              {
                id: exam.questions[1].subQuestions[1].id,
                kind: 'truefalse',
                answer: false,
                correct: false,
                mark: 0
              },
              {
                id: exam.questions[1].subQuestions[2].id,
                kind: 'truefalse',
                answer: true,
                correct: true,
                mark: 2
              },
              {
                id: exam.questions[1].subQuestions[3].id,
                kind: 'paragraph',
                answer: 'مش عارف',
                correct: false,
                mark: 0
              }
            ]
          }
        ],
        submittedAt: new Date()
      })
      .save();
    exports.push(solution2);

    for (let i = 2; i < students.length; i++) {
      let solution = await models
        .solution({
          quiz: exam.id,
          student: students[i].id,
          status: 'missed'
        })
        .save();
      exports.push(solution);
    }

    exam.locked = true;
    await exam.save();

    return exports;
  }
);
