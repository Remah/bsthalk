const models = require('../../../models');
const $baseEntity = require('./$baseEntity');
const $faker = require('../$faker');

module.exports = $baseEntity('parents', async () => {
  let exports = [];

  // 1. main parent (have multiple student for testing)
  let parent1 = await models
    .parent({
      username: 'parent',
      password: '123456',
      name: 'محسن سلامة',
      gender: 'male',
      email: 'parent@deross.com',
      phone: '01234567893',
      photo: $faker.photo('male')
    })
    .save();
  exports.push(parent1);

  // 2. fake parents
  for (let i = 1; i <= 180; i++) {
    const gender = 'male';
    const photos = {
      male:
        'https://res.cloudinary.com/derossy-backup/image/upload/v1555206304/deross-samples/placeholder-profile-male.jpg',
      female:
        'https://res.cloudinary.com/derossy-backup/image/upload/v1555206304/deross-samples/placeholder-profile-female.jpg'
    };
    const parent = await models
      .parent({
        username: 'parent' + i,
        password: '123456',
        name: $faker.fullName(gender, 2),
        gender: gender,
        email: 'parent' + i + '@deross.com',
        phone: $faker.phone(),
        photo: photos[gender]
      })
      .save();
    exports.push(parent);
  }

  return exports;
});
