const models = require('../../../models');
const $baseEntity = require('./$baseEntity');

module.exports = $baseEntity('chapters', async subjectPairs => {
  let exports = [];
  let names = [
    'عام',
    'الوحدة اﻷولي',
    'الوحدة الثانية',
    'الوحدة الثالثة',
    'الوحدة الرابعة',
    'الوحدة الخامسة'
  ];

  let pairs = subjectPairs['3'];
  for (let i = 0; i < pairs.length; i++) {
    for (let j = 0; j < names.length; j++) {
      let chapter = await new models.chapter({
        name: names[j],
        subjectPair: pairs[i].id
      }).save();
      exports.push(chapter);
    }
  }

  return exports;
});
