const models = require('../../../models');
const $baseEntity = require('./$baseEntity');
const $faker = require('../$faker');

module.exports = $baseEntity('attendance reports', async () => {
  let exports = [];

  let group = await models.group.findById(1);
  let sessions = 0;
  let payments = 0;

  for (let i = 1; i < 45; i++) {
    let rnd = $faker.$random.listItem;
    let hasHomework = rnd([true, false]);
    let reportObj = {
      group: 1,
      hasHomework: hasHomework,
      attendances: []
    };
    for (let i = 189; i <= 205; i++) {
      if (i === 190) continue;
      let attend = rnd([true, false]);
      reportObj.attendances.push({
        student: i,
        attendance: attend,
        homework: hasHomework && attend ? rnd([true, false]) : undefined
      });
    }
    const report = await new models.attendanceReport(reportObj).save();
    exports.push(report);

    sessions++;
    if (sessions > group.paymentRate) {
      sessions = 1;
      payments++;
    }
  }

  group.students.forEach(student => {
    student.sessions = sessions;
    for (let i = 0; i < payments; i++) student.payments.push({});
  });
  await group.save();

  return exports;
});
