module.exports = (entityName, entityFunc) => {
  return {
    populate: async function(...args) {
      console.log(
        `🤔 Start population of ${("[" + entityName + "]").blue}`.green
      );
      let ret = await entityFunc(...args);
      console.log(
        `🤩 Successful population of ${("[" + entityName + "]").blue} 🔥`.green
      );
      return ret;
    }
  };
};
