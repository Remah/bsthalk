const models = require('../../../models');
const $baseEntity = require('./$baseEntity');
const $faker = require('../$faker');

module.exports = $baseEntity('posts', async (teachers, students, parents) => {
  let exports = [];

  let baseUrl = 'https://res.cloudinary.com/derossy-backup/image/upload';
  let images = [
    '/v1555207115/deross-samples/posts/post1.png',
    '/v1555207110/deross-samples/posts/post2.jpg',
    '/v1555207108/deross-samples/posts/post3.png',
    '/v1555207114/deross-samples/posts/post4.png',
    '/v1555207115/deross-samples/posts/post5.png'
  ];
  let commentImg = '/v1555207113/deross-samples/posts/comment.png';

  let userIds = [0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11];
  // timeline
  for (let i = 1; i <= 15; i++) {
    let randomUser1 = i === 1 ? 0 : $faker.$random.listItem(userIds);
    let randomUser2;
    do randomUser2 = $faker.$random.listItem(userIds);
    while (randomUser2 === randomUser1);

    let post = await models
      .post({
        group: 1,
        depth: 0,
        parents: [],
        directParent: null,
        author: students[randomUser1].id,
        channel: 'students',
        images: [{ url: baseUrl + $faker.$random.listItem(images) }],
        content: 'مش فاهم الجزئية دي في المنهج - الوحدة ' + i,
        reactions: [
          { user: students[randomUser2].id, flavor: 'love' },
          { user: teachers[0].id, flavor: 'love' }
        ]
      })
      .save();

    let comment = await models
      .post({
        group: 1,
        depth: 1,
        parents: [post.id],
        directParent: post.id,
        author: students[randomUser2].id,
        channel: 'students',
        images: [{ url: baseUrl + commentImg }],
        content: 'وانا كمان',
        reactions: [
          { user: students[randomUser1].id, flavor: 'love' },
          { user: teachers[0].id, flavor: 'love' }
        ]
      })
      .save();

    let comment2 = await models
      .post({
        group: 1,
        depth: 1,
        parents: [post.id],
        directParent: post.id,
        author: teachers[0].id,
        channel: 'students',
        content: 'الجزء دا هنشرحة الحصة الجاية',
        reactions: [
          { user: students[randomUser1].id, flavor: 'love' },
          { user: students[randomUser2].id, flavor: 'love' }
        ]
      })
      .save();

    let comment3 = await models
      .post({
        group: 1,
        depth: 2,
        parents: [post.id, comment.id],
        directParent: comment.id,
        author: teachers[0].id,
        channel: 'students',
        content: 'شوف ردي تحت',
        reactions: [{ user: teachers[2].id, flavor: 'love' }]
      })
      .save();

    let comment4 = await models
      .post({
        group: 1,
        depth: 2,
        parents: [post.id, comment.id],
        directParent: comment.id,
        author: students[randomUser2].id,
        channel: 'students',
        content: 'تمام شكرا يا مستر',
        reactions: [{ user: teachers[0].id, flavor: 'love' }]
      })
      .save();

    post.comments = [comment.id, comment2.id];
    comment.comments = [comment3.id, comment4.id];

    await comment.save();
    await post.save();
  }

  for (let i = 1; i <= 15; i++) {
    let randomUser1 = i === 1 ? 0 : $faker.$random.listItem(userIds);
    let randomUser2;
    do randomUser2 = $faker.$random.listItem(userIds);
    while (randomUser2 === randomUser1);

    let post = await models
      .post({
        group: 1,
        depth: 0,
        parents: [],
        directParent: null,
        author: parents[randomUser1].id,
        channel: 'parents',
        images: [{ url: baseUrl + $faker.$random.listItem(images) }],
        content: 'ازاي حضراتكم ؟',
        reactions: [
          { user: parents[randomUser2].id, flavor: 'love' },
          { user: teachers[0].id, flavor: 'love' }
        ]
      })
      .save();

    let comment = await models
      .post({
        group: 1,
        depth: 1,
        parents: [post.id],
        directParent: post.id,
        author: parents[randomUser2].id,
        channel: 'parents',
        images: [{ url: baseUrl + commentImg }],
        content: 'بخير الحمد لله',
        reactions: [
          { user: parents[randomUser1].id, flavor: 'love' },
          { user: teachers[0].id, flavor: 'love' }
        ]
      })
      .save();

    let comment2 = await models
      .post({
        group: 1,
        depth: 1,
        parents: [post.id],
        directParent: post.id,
        author: teachers[0].id,
        channel: 'parents',
        content: 'كله تمام حضرتك',
        reactions: [
          { user: parents[randomUser1].id, flavor: 'love' },
          { user: parents[randomUser2].id, flavor: 'love' }
        ]
      })
      .save();

    let comment3 = await models
      .post({
        group: 1,
        depth: 2,
        parents: [post.id, comment.id],
        directParent: comment.id,
        author: teachers[0].id,
        channel: 'parents',
        content: 'يارب ديما',
        reactions: [{ user: teachers[2].id, flavor: 'love' }]
      })
      .save();

    let comment4 = await models
      .post({
        group: 1,
        depth: 2,
        parents: [post.id, comment.id],
        directParent: comment.id,
        author: parents[randomUser2].id,
        channel: 'parents',
        content: 'شكرا لحضرتك',
        reactions: [{ user: teachers[0].id, flavor: 'love' }]
      })
      .save();

    post.comments = [comment.id, comment2.id];
    comment.comments = [comment3.id, comment4.id];

    await comment.save();
    await post.save();
  }

  return exports;
});
