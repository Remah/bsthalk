const models = require('../../../models');
const $baseEntity = require('./$baseEntity');

module.exports = $baseEntity('groups', async subjectPairs => {
  let exports = {};

  let groups = [
    { name: 'مجموعة 1', date: 'سبت واثنين واربعاء الساعة 10 ص' },
    { name: 'مجموعة 2', date: 'سبت واثنين واربعاء الساعة 12 م' },
    { name: 'مجموعة 3', date: 'سبت واثنين واربعاء الساعة 2 م' },
    { name: 'مجموعة 4', date: 'سبت واثنين واربعاء الساعة 4 م' },
    { name: 'مجموعة 5', date: 'سبت واثنين واربعاء الساعة 6 م' },
    { name: 'مجموعة 6', date: 'سبت واثنين واربعاء الساعة 8 م' },
    { name: 'مجموعة 7', date: 'أحد وثلاثاء وخميس الساعة 10 ص' },
    { name: 'مجموعة 8', date: 'أحد وثلاثاء وخميس الساعة 12 م' },
    { name: 'مجموعة 9', date: 'أحد وثلاثاء وخميس الساعة 2 م' },
    { name: 'مجموعة 10', date: 'أحد وثلاثاء وخميس الساعة 4 م' },
    { name: 'مجموعة 11', date: 'أحد وثلاثاء وخميس الساعة 6 م' },
    { name: 'مجموعة 12', date: 'أحد وثلاثاء وخميس الساعة 8 م' }
  ];

  let teachersIds = Object.keys(subjectPairs);
  for (let i = 0; i < teachersIds.length; i++) {
    let teacherId = teachersIds[i];
    exports[teacherId] = {};
    for (let j = 0; j < subjectPairs[teacherId].length; j++) {
      let subjectId = subjectPairs[teacherId][j]._id;
      exports[teacherId][subjectId] = [];
      for (let k = 0; k < groups.length; k++) {
        let group = groups[k];
        let g = await models
          .group({
            name: group.name,
            date: group.date,
            subjectPair: parseInt(subjectId)
          })
          .save();
        exports[teacherId][subjectId].push(g);
        subjectPairs[teacherId][j].groups.push(g);
      }
      await subjectPairs[teacherId][j].save();
    }
  }

  return exports;
});
