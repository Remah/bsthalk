const $baseCtrl = require('../$baseCtrl');
const { APIResponse } = require('../../utils');
const senarios = require('./senarios');

module.exports = $baseCtrl(async (req, res) => {
  // only for development purpose
  if (process.env.NODE_ENV !== 'development') {
    return APIResponse.Forbidden(res);
  }

  await senarios.simple.populate();
  await senarios.salah.populate();

  return APIResponse.Ok(res, { message: 'senarios had been populated' });
});
