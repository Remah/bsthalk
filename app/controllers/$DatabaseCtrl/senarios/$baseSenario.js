module.exports = (senarioName, senarioFunc) => {
  return {
    populate: async function(...args) {
      console.log(
        `🤔 Start senario population of ${('[' + senarioName + ']').blue}`.green
      );
      let ret = await senarioFunc(...args);
      console.log(
        `🤩 Successful senario population of ${
          ('[' + senarioName + ']').blue
        } 🔥`.green
      );
      return ret;
    }
  };
};
