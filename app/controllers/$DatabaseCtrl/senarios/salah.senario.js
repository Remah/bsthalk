const $baseSenario = require('./$baseSenario');
const $faker = require('../$faker');

module.exports = $baseSenario('simple', async () => {
  const models = require('../../../models');
  const baseUrl = 'https://res.cloudinary.com/derossy-backup/image/upload';

  let teacher = await models
    .teacher({
      username: 'MohamedSalah',
      password: 'password',
      name: 'محمد صلاح',
      gender: 'male',
      email: 'MohamedSalah@deross.com',
      phone: '01112099886',
      photo: baseUrl + '/v1555206304/deross-samples/male-teacher1.png',
      premium: true
    })
    .save();

  for (let i = 5; i <= 9; i++) {
    const subjectPair = await models
      .subjectPair({
        teacher: teacher.id,
        subject: 1,
        level: i
      })
      .save();

    const chapter = await new models.chapter({
      name: 'عام', // [TODO] en
      subjectPair: subjectPair.id
    }).save();
  }
});
