const mongoose = require('mongoose');
const $baseCtrl = require('../$baseCtrl');
const { APIResponse } = require('../../utils');

module.exports = $baseCtrl(async (req, res, next) => {
  // only for development purpose
  if (process.env.NODE_ENV !== 'development') {
    return APIResponse.Forbidden(res);
  }

  try {
    await mongoose.connection.dropDatabase();
  } catch (error) {
    return next(error);
  }

  // [TODO] auto restart app
  APIResponse.Ok(res, { message: 'database had been dropped' });
  return process.exit(1);
});
