let _registry = { fullnames: [], phones: [] };

const baseUrl = 'https://res.cloudinary.com/derossy-backup/image/upload';
const maleNames = [
  'أحمد',
  'محمد',
  'محمود',
  'محمود',
  'كريم',
  'جمال',
  'هشام',
  'يحيي',
  'خالد',
  'توفيق',
  'فريد',
  'عمر',
  'باسل',
  'باسم',
  'شريف',
  'شادي',
  'فؤاد',
  'فاروق',
  'ياسر',
  'عبد الله',
  'عبد الرحمن',
  'جلال',
  'مازن',
  'فوزي',
  'محسن',
  'سيد'
];
const femalNames = [
  'شروق',
  'سارة',
  'ثناء',
  'مروة',
  'مريم',
  'إسراء',
  'أسماء',
  'بسمة',
  'خلود',
  'شيماء',
  'ميرنا',
  'نادين',
  'ندي',
  'نهي',
  'هدير',
  'سميرة',
  'ساجدة',
  'دعاء',
  'سامية'
];
const malePhotos = [
  '/v1555206303/deross-samples/male1.png',
  '/v1555206303/deross-samples/male2.png',
  '/v1555206303/deross-samples/male3.png',
  '/v1555206304/deross-samples/male4.png',
  '/v1555206304/deross-samples/male5.png'
];
const femalePhotos = [
  '/v1555206302/deross-samples/female1.png',
  '/v1555206302/deross-samples/female2.png',
  '/v1555206302/deross-samples/female3.png',
  '/v1555206302/deross-samples/female4.png',
  '/v1555206303/deross-samples/female5.png'
];

const $faker = {
  fullName: (gender, length = 3, suffix = null) => {
    if (gender !== 'male' && gender !== 'female') return null;
    if (length < 1) return null;
    let name = '';
    do {
      name = '';
      if (gender === 'female') {
        name += $faker.$random.listItem(femalNames);
        length--;
      }
      for (let i = 0; i < length; i++) {
        if (i !== 0 || gender === 'female') name += ' ';
        name += $faker.$random.listItem(maleNames);
      }
      if (suffix) name += ' ' + suffix;
    } while (_registry.fullnames.indexOf(name) !== -1);
    _registry.fullnames.push(name);
    return name;
  },
  photo: gender => {
    if (gender !== 'male' && gender !== 'female') return null;
    if (gender === 'male') {
      return baseUrl + $faker.$random.listItem(malePhotos);
    } else if (gender === 'female') {
      return baseUrl + $faker.$random.listItem(femalePhotos);
    }
  },
  phone: () => {
    let phone = '';
    do {
      phone = '01';
      phone += $faker.$random.listItem([0, 1, 2, 5]);
      for (let i = 1; i <= 8; i++) {
        phone += $faker.$random.integer(0, 9);
      }
    } while (_registry.phones.indexOf(phone) !== -1);
    _registry.phones.push(phone);
    return phone;
  },
  $random: {
    integer: (min, max) => {
      min = Math.ceil(min);
      max = Math.floor(max);
      return Math.floor(Math.random() * (max - min + 1)) + min;
    },
    listItem: list => list[$faker.$random.integer(0, list.length - 1)]
  }
};

module.exports = $faker;
