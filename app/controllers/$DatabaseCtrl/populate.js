const $baseCtrl = require('../$baseCtrl');
const { APIResponse } = require('../../utils');
const entites = require('./entites');

module.exports = $baseCtrl(async (req, res) => {
  // only for development purpose
  if (process.env.NODE_ENV !== 'development') {
    return APIResponse.Forbidden(res);
  }

  const levels = await entites.level.populate();
  const subjects = await entites.subject.populate();
  const admins = await entites.admin.populate();
  const teachers = await entites.teacher.populate();
  const parents = await entites.parent.populate();
  const subjectPairs = await entites.subjectPair.populate(teachers);
  const groups = await entites.group.populate(subjectPairs);
  const students = await entites.student.populate(parents, groups);
  const attendanceReports = await entites.attendanceReport.populate();
  const chpaters = await entites.chapter.populate(subjectPairs);
  const tutorials = await entites.tutorial.populate(chpaters);
  const posts = await entites.post.populate(teachers, students, parents);
  const examsAndHomeworks = await entites.examAndHomework.populate();
  const solutions = await entites.solution.populate(
    examsAndHomeworks,
    subjectPairs,
    students
  );
  const questions = await entites.question.populate();

  return APIResponse.Ok(res, { message: 'database had been populated' });
});
