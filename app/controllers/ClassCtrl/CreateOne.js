const $baseCtrl = require('../$baseCtrl');
const models = require('../../models');
const { APIResponse } = require('../../utils');
const { isUndefined } = require('util');

module.exports = $baseCtrl(async (req, res, next) => {

    // check if values entered by user
    if (isUndefined(req.body.name) || isUndefined(req.body.level)) {
        return APIResponse.BadRequest(res, "You have to fill all options");
    }

    // create class 
    const newClass = await new models.class(req.body).save();

    const response = {
        newClass
    };

    return APIResponse.Created(res, response)
});
