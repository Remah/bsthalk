const { isUndefined } = require('util');
const $baseCtrl = require('../$baseCtrl');
const models = require('../../models');
const { APIResponse } = require('../../utils');
const cloudinaryStorage = require('../../services/cloudinaryStorage');

module.exports = $baseCtrl(async (req, res) => {
    // fetch specific class by id 
    const Class = await models.class.findById(req.params.id)

    return APIResponse.Ok(res, Class)
});
