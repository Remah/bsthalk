const $baseCtrl = require('../$baseCtrl');
const models = require('../../models');
const { APIResponse } = require('../../utils');

module.exports = $baseCtrl(async (req, res, next) => {
    // fetch all classes in db
    const classes = await models.class.find()

    return APIResponse.Ok(res, classes)
});
