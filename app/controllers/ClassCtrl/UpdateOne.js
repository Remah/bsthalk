const { isUndefined } = require('util');
const $baseCtrl = require('../$baseCtrl');
const models = require('../../models');
const { APIResponse } = require('../../utils');
const cloudinaryStorage = require('../../services/cloudinaryStorage');

module.exports = $baseCtrl(async (req, res) => {

    // fetch specific class by id
    const Class = await models.class.findById(req.params.id)

    // check only the values which need to updated
    if (req.body.level) Class.level = req.body.level
    if (req.body.name) Class.name = req.body.name

    // save changes
    await Class.save()

    return APIResponse.Ok(res, Class)
});
