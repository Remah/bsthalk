const { isUndefined } = require('util');
const $baseCtrl = require('../$baseCtrl');
const models = require('../../models');
const { APIResponse } = require('../../utils');
const cloudinaryStorage = require('../../services/cloudinaryStorage');

module.exports = $baseCtrl(async (req, res) => {

    // catch specific class
    const Class = await models.class.findById(req.params.id)

    // delete class
    await Class.delete()

    return APIResponse.NoContent(res)
});
