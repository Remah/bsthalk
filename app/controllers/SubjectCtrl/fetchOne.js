const { isUndefined } = require('util');
const $baseCtrl = require('../$baseCtrl');
const models = require('../../models');
const { APIResponse } = require('../../utils');
const cloudinaryStorage = require('../../services/cloudinaryStorage');

module.exports = $baseCtrl(async (req, res) => {
    // fetch specific subject and populate it by its class
    const subject = await models.subject.findById(req.params.id).populate('Class')

    return APIResponse.Ok(res, subject)
});