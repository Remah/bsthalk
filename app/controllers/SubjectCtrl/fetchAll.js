const $baseCtrl = require('../$baseCtrl');
const models = require('../../models');
const { APIResponse } = require('../../utils');

module.exports = $baseCtrl(async (req, res, next) => {

    // fetch all subjects 
    const subjects = await models.subject.find()

    return APIResponse.Ok(res, subjects)
});
