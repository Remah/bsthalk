const { isUndefined } = require('util');
const $baseCtrl = require('../$baseCtrl');
const models = require('../../models');
const { APIResponse } = require('../../utils');
const cloudinaryStorage = require('../../services/cloudinaryStorage');

module.exports = $baseCtrl(async (req, res) => {
    // fetch specific subject by id
    const subject = await models.subject.findById(req.params.id)
    // delete subject
    await subject.delete()

    return APIResponse.NoContent(res)
});
