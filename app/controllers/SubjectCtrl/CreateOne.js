const $baseCtrl = require('../$baseCtrl');
const models = require('../../models');
const { APIResponse } = require('../../utils');
const { isUndefined } = require('util');
const cloudinaryStorage = require('../../services/cloudinaryStorage');


module.exports = $baseCtrl(
    [{ name: 'icon', maxCount: 1 }],
    cloudinaryStorage,
    async (req, res, next) => {

        // check if values entered by user
        if (
            isUndefined(req.body.Class) ||
            isUndefined(req.body.name)
        ) {
            return APIResponse.BadRequest(res, "You have to fill all options");
        }

        // check if user upload photo for subject
        if (req.files && req.files['icon']) {
            req.body.icon = req.files['icon'][0].secure_url;
        }

        // create new subject
        const newSubject = await new models.subject(req.body).save();

        const response = {
            newSubject
        };

        return APIResponse.Created(res, response)
    });
