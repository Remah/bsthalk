const { isUndefined } = require('util');
const $baseCtrl = require('../$baseCtrl');
const models = require('../../models');
const { APIResponse } = require('../../utils');
const cloudinaryStorage = require('../../services/cloudinaryStorage');

// [TODO] update icon 
module.exports = $baseCtrl(
    [{ name: 'icon', maxCount: 1 }],
    cloudinaryStorage,
    async (req, res) => {

        // fetch specif subject
        const subject = await models.subject.findById(req.params.id)

        // check only values need to updated
        if (req.body.name) subject.name = req.body.name

        // check if user want to update photo
        if (req.files && req.files['icon']) {
            subject.icon = req.files['icon'][0].secure_url;
        }

        await subject.save()
        return APIResponse.Ok(res, subject)
    });
