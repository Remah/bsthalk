const $baseCtrl = require('../$baseCtrl');
const { APIResponse } = require('../../utils');

module.exports = $baseCtrl(async (req, res) => {
  return APIResponse.Ok(res, {
    user: req.authenticatedUser,
    ping: 'pong'
  });
});
