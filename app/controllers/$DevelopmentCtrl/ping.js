const $baseCtrl = require('../$baseCtrl');
const models = require('../../models');
const { APIResponse } = require('../../utils');
const cloudinaryStorage = require('../../services/cloudinaryStorage');

module.exports = $baseCtrl(
  [{ name: 'photo', maxCount: 1 }],
  cloudinaryStorage,
  async (req, res) => {
    return APIResponse.Ok(res, {
      ping: 'pong',
      photo: req.files ? req.files['photo'][0].secure_url : null
    });
  }
);
