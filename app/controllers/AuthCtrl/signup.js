const jwt = require('jsonwebtoken');
const { isUndefined } = require('util');
const $baseCtrl = require('../$baseCtrl');
const models = require('../../models');
const { APIResponse } = require('../../utils');
const bcrypt = require('bcryptjs')
const CodeGenerator = require('node-code-generator');
const generator = new CodeGenerator();
const nodemailer = require('nodemailer');
const cloudinaryStorage = require('../../services/cloudinaryStorage');

// config gmail
const transproter = nodemailer.createTransport({
  service: 'Gmail',
  auth: {
    user: 'remah3335@gmail.com',
    pass: 'remah654312'
  }
});


module.exports = $baseCtrl(
  [{ name: 'photo', maxCount: 1 }],
  cloudinaryStorage,
  async (req, res) => {
    // Check if values not entered
    if (
      isUndefined(req.body.username) ||
      isUndefined(req.body.password) ||
      isUndefined(req.body.email) ||
      isUndefined(req.body.gender) ||
      isUndefined(req.body.gender) ||
      isUndefined(req.body.level) ||
      isUndefined(req.body.class) ||
      isUndefined(req.body.semester)
    ) {
      return APIResponse.BadRequest(res, "You have to fill all options");
    }

    // Check if E-mail Already Exist
    const user = await models._user.findOne({ email: req.body.email });
    if (user) {
      return APIResponse.BadRequest(res, " Email Already in use .")
    }

    // Encrypt Password
    var salt = bcrypt.genSaltSync(10);
    var hash = bcrypt.hashSync(req.body.password, salt);
    req.body.password = hash

    // Upload photo if enter by user
    if (req.files && req.files['photo']) {
      req.body.photo = req.files['photo'][0].secure_url;
    }

    // generate random code **** , and send to user
    const newCode = generator.generateCodes('#+#+', 100)[0];
    transproter.sendMail({
      to: req.body.email,
      from: 'remah3335@gmail.com',
      subject: 'verification code',
      text: ` your verification code is ${newCode}`
    })
    req.body.code = newCode

    // save user to db
    const newStudent = await new models.student(req.body).save();

    const response = {
      user: newStudent
    };

    return APIResponse.Created(res, response)
  });
