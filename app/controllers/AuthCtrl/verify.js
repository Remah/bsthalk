const jwt = require('jsonwebtoken');
const { isUndefined } = require('util');
const $baseCtrl = require('../$baseCtrl');
const models = require('../../models');
const { APIResponse } = require('../../utils');



module.exports = $baseCtrl(async (req, res) => {
    // retreive specific user by Id
    const user = await models._user.findById(req.params.id)

    // check if code which send to user is equal to code which entered by him 
    if (user.code === req.body.code) {
        user.enabled = true
        user.code = undefined
        await user.save()
        return APIResponse.Ok(res, 'USER ENABLED .')
    }
    // If code is not correct 
    return APIResponse.Unauthorized(res, 'Verification code is incorrect .')
});
