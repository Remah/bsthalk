const jwt = require('jsonwebtoken');
const { isUndefined } = require('util');
const $baseCtrl = require('../$baseCtrl');
const models = require('../../models');
const { APIResponse } = require('../../utils');
const bcrypt = require('bcryptjs')


module.exports = $baseCtrl(async (req, res) => {

    // Check if values not entered by user
    if (isUndefined(req.body.email) || isUndefined(req.body.password)) {
        return APIResponse.BadRequest(res, "You have to fill all options");
    }

    // check if email already exist
    const user = await models._user.findOne({ email: req.body.email });
    if (!user) {
        return APIResponse.Unauthorized(res, 'Invalid email or password 1');
    }
    // check if password correct
    const isMatch = await bcrypt.compare(req.body.password, user.password)
    if (!isMatch) {
        return APIResponse.Unauthorized(res, 'Invalid email or password');
    }

    // if user not enter code , his email still disabled
    if (!user.enabled) {
        return APIResponse.Unauthorized(res, 'Your account is disabled');
    }


    let payload = { userId: user.id };
    let options = { expiresIn: 24 * 30 * 6 + 'h' }; // 6 monthes
    let token = jwt.sign(payload, process.env.JWT_SECRET, options);

    const response = {
        token: token,
        user: user
    };

    if (!user.used) {
        user.used = true;
        await user.save();
    }

    return APIResponse.Ok(res, response);
});
