const express = require("express");
const policies = require("../../policies");
const ctrls = require("../../controllers");

let router = express.Router();


router.get('/classes', ctrls.ClassCtrl.fetchAll)
router.get('/class/:id', ctrls.ClassCtrl.fetchOne)
router.post('/class', ctrls.ClassCtrl.CreateOne)
router.put('/class/:id', ctrls.ClassCtrl.UpdateOne)
router.delete('/class/:id', ctrls.ClassCtrl.DeleteOne)

module.exports = router;
