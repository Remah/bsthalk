const express = require("express");
const policies = require("../../policies");
const ctrls = require("../../controllers");

let router = express.Router();


router.get('/subjects', ctrls.SubjectCtrl.fetchAll)
router.get('/subject/:id', ctrls.SubjectCtrl.fetchOne)
router.post('/subject', ctrls.SubjectCtrl.CreateOne)
router.put('/subject/:id', ctrls.SubjectCtrl.UpdateOne)
router.delete('/subject/:id', ctrls.SubjectCtrl.DeleteOne)

module.exports = router;
