const express = require('express');
const policies = require('../policies');
const ctrls = require('../controllers');
const resources = require('./resources');

let apiRouter = express.Router();

// public
apiRouter.delete('/database', ctrls.$DatabaseCtrl.drop); // dev
apiRouter.post('/database', ctrls.$DatabaseCtrl.populate); // dev
apiRouter.post('/senarios', ctrls.$DatabaseCtrl.senario); // dev
apiRouter.get('/ping', ctrls.$DevelopmentCtrl.ping); // dev
apiRouter.post('/signup', ctrls.AuthCtrl.signup);
apiRouter.post('/login', ctrls.AuthCtrl.login);
apiRouter.post('/verify/:id', ctrls.AuthCtrl.verify)

// private
apiRouter.use(policies.isAuthenticated);
apiRouter.get('/private-ping', ctrls.$DevelopmentCtrl.privatePing); // dev

// populate all resources
for (let key of Object.keys(resources)) {
  let resource = resources[key];
  apiRouter.use(resource);
}

module.exports = apiRouter;
