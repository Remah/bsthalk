const mongoose = require('mongoose');
const $baseModel = require('./$baseModel');

const schema = new mongoose.Schema(
    {
        name: String,
        Class: {
            type: Number,
            ref: 'class'
        },
        icon: {
            type: String,
            default:
                'https://res.cloudinary.com/derossy-backup/image/upload/v1555206304/deross-samples/placeholder-profile-male.jpg'
        }
    },
    { timestamps: true }
);

const response = doc => {
    return {
        id: doc.id,
        name: doc.name,
        icon: doc.icon,
        Class: doc.Class,
        createdAt: doc.createdAt,
        updatedAt: doc.updatedAt
    };
};

module.exports = $baseModel('subject', schema, {
    responseFunc: response
});
