const mongoose = require('mongoose');
const UserModel = require('./_user.model');

const schema = new mongoose.Schema(
  {
    premium: {
      type: Boolean,
      default: false
    }
  },
  { discriminatorKey: 'role' }
);
module.exports = UserModel.discriminator('teacher', schema);
