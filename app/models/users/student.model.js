const mongoose = require('mongoose');
const $baseSchema = require('../$baseSchema');
const UserModel = require('./_user.model');

const schema = new mongoose.Schema({}, { discriminatorKey: 'role' }
);

module.exports = UserModel.discriminator(
  'student',
  $baseSchema('student', schema)
);
