const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const RandExp = require('randexp');
const notificationService = require('../../services/notification');
const $baseModel = require('../$baseModel');

// RegExp rules
const usernameRules = /^[a-zA-Z][a-zA-Z0-9]{4,19}$/;
const passwordRules = /^.{6,}$/;
const emailRules = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
const phoneRules = /^01[0125][0-9]{8}$/;

// [TODO] fix partial unique index, `on admin only`
const schema = new mongoose.Schema(
  {
    username: {
      type: String,
      match: usernameRules,
      // required: true,
      unique: true,
      index: true,
      sparse: true
    },
    password: {
      type: String,
      match: passwordRules,
      // required: true
    },
    gender: {
      type: String,
      enum: ['male', 'female', null],
      default: null
    },
    email: {
      type: String,
      match: emailRules,
      unique: true,
      index: true,
      sparse: true
    },
    phone: {
      type: String,
      match: phoneRules,
      unique: true,
      index: true,
      sparse: true
    },
    photo: {
      type: String,
      default:
        'https://res.cloudinary.com/derossy-backup/image/upload/v1555206304/deross-samples/placeholder-profile-male.jpg'
    },
    enabled: {
      type: Boolean,
      default: false
    },
    level: {
      type: String
    },
    class: {
      type: String
    },
    semester: {
      type: String
    },
    code: {
      type: String
    },
    city: {
      type: String
    }
  },
  { timestamps: true, discriminatorKey: 'role' }
);

const response = (doc, options) => {
  let groups = doc.groups;
  if (doc.groups && typeof doc.groups[0] === 'object') {
    groups = doc.groups.map(group => group.toJSON({ student: doc.id }));
  }

  return {
    id: doc.id,
    username: doc.username,
    password: options.password && !doc.activated ? doc.password : undefined,
    name: doc.name,
    gender: doc.gender,
    photo: doc.photo,
    email: doc.email,
    phone: doc.phone,
    level: doc.level, // student
    parent:
      typeof doc.parent === 'object' ? doc.parent.toJSON(options) : doc.parent, // student
    groups: groups, // student
    premium: doc.premium, // teacher
    enabled: doc.enabled,
    activated: doc.activated,
    used: doc.used,
    createdAt: doc.createdAt,
    updatedAt: doc.updatedAt
  };
};

schema.pre('save', async function (next) {
  const user = this;

  let nullableFields = ['phone', 'email'];
  for (let i = 0; i < nullableFields.length; i++) {
    if (user.isModified(nullableFields[i])) {
      const value = user[nullableFields[i]];
      if (value === '' || value === null) user[nullableFields[i]] = undefined;
    }
  }

  let uniqueFields = ['username', 'email', 'phone'];
  for (let i = 0; i < uniqueFields.length; i++) {
    if (user.isModified(uniqueFields[i])) {
      let value = user[uniqueFields[i]];
      if (value === undefined) continue;
      let filter = {};
      filter[uniqueFields[i]] = value;
      let count = await mongoose.model('user').count(filter);
      if (count) {
        let error = new mongoose.Error.ValidationError(user);
        error.errors[uniqueFields[i]] = {
          message: `(${value}) is not a unique value`
        };
        throw error;
      }
    }
  }

  if (!user.isModified('password')) return next();
  if (!user.activated) return next();
  try {
    let hash = await bcrypt.hash(user.password, 10);
    user.activated = true;
    user.password = hash;
    return next();
  } catch (err) {
    return next(err);
  }
});

schema.methods.isValidPassword = async function (password) {
  const user = this;
  if (!user.activated) {
    return user.password === password;
  }
  return bcrypt.compare(password, user.password);
};

schema.methods.sendNotification = async function (message) {
  let changed = false;
  let len = this.pushTokens.length;
  while (len--) {
    const deviceToken = this.pushTokens[len].deviceToken;
    try {
      await notificationService.sendNotification(deviceToken, message);
    } catch (error) {
      this.pushTokens.splice(len, 1);
      changed = true;
    }
  }
  if (changed) await this.save();
};

schema.statics.generateRandomUsername = async function () {
  let username;
  do {
    username = new RandExp(/^[a-z][a-z0-9]{6}$/).gen();
  } while (await this.findOne({ username: username }));
  return username;
};

schema.statics.generateRandomPassword = async function () {
  return new RandExp(/^[a-z0-9]{6}$/).gen();
};

module.exports = $baseModel('user', schema, {
  responseFunc: response
});
