const mongoose = require('mongoose');
const $baseModel = require('./$baseModel');

const schema = new mongoose.Schema(
    {
        name: String,
        level: {
            type: String,
            enum: ['High', 'Middle', 'Low']
        }
    },
    { timestamps: true }
);

const response = doc => {
    return {
        id: doc.id,
        name: doc.name,
        level: doc.level,
        createdAt: doc.createdAt,
        updatedAt: doc.updatedAt
    };
};

module.exports = $baseModel('class', schema, {
    responseFunc: response
});
