module.exports = {
  _user: require('./users/_user.model'),
  admin: require('./users/admin.model'),
  teacher: require('./users/teacher.model'),
  parent: require('./users/parent.model'),
  student: require('./users/student.model'),
  class: require('./class.model'),
  subject: require('./subject.model'),
};
