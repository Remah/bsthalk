const jwt = require('jsonwebtoken');
const { isUndefined } = require('util');
const { APIResponse } = require('../utils');
const models = require('../models');

// jwt Auth Checker
module.exports = (req, res, next) => {
  req.authenticatedUser = null;

  if (!req.token) {
    return next();
  }

  return jwt.verify(req.token, process.env.JWT_SECRET, async (err, decoded) => {
    // [TOOD] send old token in login endpoint cause unauthorized bug
    // handle JWT parsing errors
    if (err) {
      if (err.name) {
        if (err.name === 'JsonWebTokenError') {
          return APIResponse.Unauthorized(res, 'Invalid access token.');
        } else if (err.name === 'TokenExpiredError') {
          return APIResponse.Unauthorized(
            res,
            'Expired access token, please login again.'
          );
        }
      }
      return APIResponse.ServerError(res, err, err.message);
    }

    if (isUndefined(decoded.userId)) {
      return APIResponse.Unauthorized(res, 'Invalid access token.');
    }

    let user = await models._user.findById(decoded.userId);

    if (
      process.env.NODE_ENV !== 'development' &&
      user.role !== req.apiKeyRole
    ) {
      return APIResponse.Unauthorized(res);
    }

    if (user && !user.enabled) {
      return APIResponse.Unauthorized(res, 'Your account is disabled');
    }

    req.authenticatedUser = user;
    return next();
  });
};
